describe("Movie Search", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");
    cy.intercept("GET", "https://api.themoviedb.org/3/search/movie?*", {
      fixture: "../../fixtures/movie-list-star-wars.json",
    }).as("fetchMovieList");

    cy.intercept("GET", "https://api.themoviedb.org/3/movie/11?*", {
      fixture: "../../fixtures/movie-detail-star-wars.json",
    }).as("fetchMovieDetail");
  });

  describe("Given a search input", () => {
    it("returns a list with movie results and its pagination", () => {
      cy.get("[data-testid=search-input]").clear().type("Star Wars");
      cy.get("[data-testid=search-btn]").click();

      cy.wait("@fetchMovieList");

      cy.get("[data-testid=moviecard]").should("have.length", 20);
      cy.get("[data-testid=moviecard-title]")
        .first()
        .should("have.text", "Star Wars");
      cy.get("[data-testid=moviecard-date]")
        .first()
        .should("have.text", "Release Date: 25 May 1977");
      cy.get("[data-testid=pagination-btn-prev]").should("be.disabled");
      cy.get("[data-testid=pagination-btn-first]").should("be.disabled");
      cy.get("[data-testid=pagination-btn-next]").should("not.be.disabled");
      cy.get("[data-testid=pagination-btn-last]").should("not.be.disabled");
      cy.get("[data-testid=pagination-txt-current]").should("have.text", "1");
    });
  });

  describe("When clicking on the pagination buttons", () => {
    it("enables and disables the first, prev, next and last buttons correctly", () => {
      const buttonLast = "[data-testid=pagination-btn-last]";

      cy.get("[data-testid=search-input]").clear().type("Star Wars");
      cy.get("[data-testid=search-btn]").click();

      cy.wait("@fetchMovieList");

      cy.get(buttonLast).click();

      cy.wait("@fetchMovieList");

      cy.get("[data-testid=pagination-btn-first]").should("not.be.disabled");
      cy.get("[data-testid=pagination-btn-prev]").should("not.be.disabled");
      cy.get("[data-testid=pagination-btn-next]").should("be.disabled");
      cy.get(buttonLast).should("be.disabled");
    });
  });

  describe("Given a movie card", () => {
    it("renders the movie detail page when clicking on the card", () => {
      cy.get("[data-testid=search-input]").clear().type("Star Wars");
      cy.get("[data-testid=search-btn]").click();

      cy.wait("@fetchMovieList");

      cy.get("[data-testid=moviecard]").first().click();

      cy.wait("@fetchMovieDetail");

      cy.get("[data-testid=moviedetail-title]").should(
        "have.text",
        "Star Wars"
      );
      cy.get("[data-testid=moviedetail-tagline]").should(
        "have.text",
        "Tagline: A long time ago in a galaxy far, far away..."
      );
    });
  });
});
