# The Movie Search App

This is a (WIP) React frontend for the [TMDB API](https://www.themoviedb.org/).

## Getting started

```
npm install
npm run start
```

Please check [package.json](package.json) for the other available scripts.
