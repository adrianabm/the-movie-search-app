import React from "react";
import "whatwg-fetch";
import "@testing-library/jest-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { render, screen, waitFor } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import MovieSearchPage from "./MovieSearchPage";
import MovieListFixture from "../../fixtures/movie-list-star-wars.json";

const server = setupServer(
  rest.get("https://api.themoviedb.org/3/*", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(MovieListFixture));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("Successful response with code 200", () => {
  it("Fetches and renders a movie list", async () => {
    render(
      <MemoryRouter initialEntries={["/?search=Star+Wars&page=1"]}>
        <MovieSearchPage />
      </MemoryRouter>
    );

    await waitFor(() => {
      expect(screen.getAllByTestId("moviecard-title")[0]).toHaveTextContent(
        "Star Wars"
      );
      expect(screen.getAllByTestId("moviecard-date")[0]).toHaveTextContent(
        "Release Date: 25 May 1977"
      );
    });
  });
});
