import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styled from "styled-components";
import MovieReviewList from "../components/MovieReviews/MovieReviewList";
import PrimaryButton from "../components/styled/PrimaryButton";
import movieApiClient from "../utils/movieApiClient";
import {
  ApiError,
  FullMovieResponse,
  ImageSize,
  isApiError,
} from "../utils/typesApi";

export default function MovieDetailPage() {
  const { id } = useParams() as { id: string };
  const [error, setFetchError] = useState<ApiError | null>();

  const [movieData, setMovieData] = useState<FullMovieResponse | null>();

  useEffect(() => {
    movieApiClient.getMovieDetail(id).then((data) => {
      if (isApiError(data)) {
        setFetchError(error);
      } else {
        setMovieData(data);
      }
    });
  }, []);

  return (
    <PageContainer>
      <div style={{ display: "flex" }}>
        {movieData ? (
          <MoviePoster
            src={movieApiClient.buildMovieImageUrl(
              movieData.poster_path,
              ImageSize.LARGE
            )}
          ></MoviePoster>
        ) : (
          <MoviePoster></MoviePoster>
        )}
        <MovieDetailWrapper>
          <h1 data-testid="moviedetail-title">{movieData?.title}</h1>
          <p data-testid="moviedetail-tagline">Tagline: {movieData?.tagline}</p>
          <p>Rating: {movieData?.vote_average}</p>
          <p>Plot: {movieData?.overview}</p>
        </MovieDetailWrapper>
      </div>
      <PrimaryButton onClick={() => history.back()}>Back</PrimaryButton>
      <MovieReviewList movieId={id} />
    </PageContainer>
  );
}

export const PageContainer = styled.div`
  max-width: 1300px;
`;

const MoviePoster = styled.img`
  width: 30%;
  margin-right: 40px;
  margin-bottom: 40px;
  margin-top: 40px;
`;

const MovieDetailWrapper = styled.div`
  padding: 20px;
`;
