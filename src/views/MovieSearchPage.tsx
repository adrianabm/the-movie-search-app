import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import styled from "styled-components";
import MovieList from "../components/SearchResults/MovieList";
import Pagination from "../components/SearchResults/Pagination";
import SearchBar from "../components/SearchBar";
import TrendingNow from "../components/TrendingNow";
import movieApiClient from "../utils/movieApiClient";
import { ApiError, isApiError, Movie } from "../utils/typesApi";
import MovieListLoader from "../components/SearchResults/MovieListLoader";

export default function MovieSearchPage() {
  const [searchParams, setSearchParams] = useSearchParams();
  const searchInputParam = searchParams.get("search") || "";
  const currentPageParam = Number(searchParams.get("page")) || 1;

  const [searchInput, setSearchInput] = useState<string>(searchInputParam);
  const [movieList, setMovieList] = useState<Movie[]>([]);
  const [error, setFetchError] = useState<ApiError | null>();
  const [currentPage, setCurrentPage] = useState<number>(currentPageParam);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  async function getMovies() {
    if (!searchInput) return;

    setSearchParams({ search: searchInput, page: currentPage.toString() });
    setIsLoading(true);

    const response = await movieApiClient.getMovieList(
      searchInput,
      currentPage
    );
    if (isApiError(response)) {
      setFetchError(response);
    } else {
      setMovieList(response.results);
      setTotalPages(response.total_pages);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    getMovies();
  }, [currentPage, searchParams]);

  return (
    <PageContainer>
      <SearchBar
        searchInput={searchInput}
        setSearchInput={setSearchInput}
        onClick={() => setSearchParams({ search: searchInput })}
      />
      {isLoading && <MovieListLoader />}
      {movieList.length > 0 && (
        <>
          <MovieList movieList={movieList} error={error} />
          <Pagination
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            lastPage={totalPages}
          />
        </>
      )}
      <TrendingNow />
    </PageContainer>
  );
}

export const PageContainer = styled.div`
  max-width: 1300px;
`;
