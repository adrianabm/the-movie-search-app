import React from "react";
import "whatwg-fetch";
import "@testing-library/jest-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { act, render, screen, waitFor } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import MovieDetailPage from "./MovieDetailPage";
import MovieDetailFixture from "../../fixtures/movie-detail-star-wars.json";

const server = setupServer(
  rest.get("https://api.themoviedb.org/3/*", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(MovieDetailFixture));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("Successful response with code 200", () => {
  it("Fetches and renders details of a movie", async () => {
    act(() => {
      render(
        <BrowserRouter>
          <MovieDetailPage />
        </BrowserRouter>
      );
    });

    await waitFor(() => {
      expect(screen.getByTestId("moviedetail-title")).toHaveTextContent(
        "Star Wars"
      );
      expect(screen.getByTestId("moviedetail-tagline")).toHaveTextContent(
        "Tagline: A long time ago in a galaxy far, far away..."
      );
    });
  });
});
