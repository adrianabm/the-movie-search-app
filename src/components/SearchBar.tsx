import React, { MouseEventHandler } from "react";
import styled from "styled-components";
import backgroundImage from "../assets/search-header.png";
import PrimaryButton from "./styled/PrimaryButton";

interface ISearchBarProps {
  searchInput: string;
  setSearchInput: (input: string) => void;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

export default function SearchBar({
  searchInput,
  setSearchInput,
  onClick,
}: ISearchBarProps) {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setSearchInput(event.target.value);

  return (
    <SearchBarContainer>
      <SearchBarTitle>Welcome.</SearchBarTitle>
      <SearchBarSubTitle>
        Millions of movies, TV shows and people to discover. Explore now.
      </SearchBarSubTitle>
      <SearchWrapper>
        <SearchInput
          // TODO: implement change on Enter key press
          value={searchInput}
          onChange={handleChange}
          data-testid="search-input"
        ></SearchInput>
        <PrimaryButton onClick={onClick} data-testid="search-btn">
          Search
        </PrimaryButton>
      </SearchWrapper>
    </SearchBarContainer>
  );
}

const SearchBarContainer = styled.div`
  height: 300px;
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  background-image: url("${backgroundImage}");
  padding: 20px;
  box-sizing: border-box;
  margin-bottom: 20px;
`;

const SearchBarTitle = styled.h2`
  font-size: 3em;
  font-weight: 700;
  line-height: 1;
  color: white;
  margin-bottom: 10px;
`;

const SearchBarSubTitle = styled.h3`
  font-size: 2em;
  font-weight: 600;
  margin: 0;
  color: white;
  margin-bottom: 40px;
`;

const SearchInput = styled.input`
  display: flex;
  border-radius: 0px;
  border-width: 0px;
  height: 40px;
  flex-grow: 1;
  padding: 0px;
  margin-right: 10px;
  padding-left: 10px;
  font-size: 1rem;
  color: #636e72;
  font-weight: 300;
`;

const SearchWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: flex-end;
`;
