import React from "react";
import formatDate from "../../utils/formatDate";
import { MovieReview } from "../../utils/typesApi";

export default function MovieReviewCard({ review }: { review: MovieReview }) {
  const dateFormatted = formatDate(review.created_at);

  return (
    <div>
      <p>
        Review by{" "}
        <strong data-testid="review-card-username">
          {review.author_details.username}
        </strong>
      </p>
      <p data-testid="review-card-content">{review.content}</p>
      <p data-testid="review-card-date">Date: {dateFormatted}</p>
    </div>
  );
}
