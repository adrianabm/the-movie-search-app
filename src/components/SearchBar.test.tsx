import React from "react";
import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import SearchBar from "./SearchBar";

describe("SearchBar", () => {
  it("Renders an input element with given value and a search button", async () => {
    render(
      <SearchBar
        searchInput={"Star wars"}
        setSearchInput={() => {}}
        onClick={() => {}}
      />
    );

    expect(screen.getByTestId("search-input")).toHaveValue("Star wars");
    expect(screen.getByTestId("search-btn")).toBeInTheDocument();
  });

  it("Calls setSearchInput on input change", async () => {
    const setSearchInput = jest.fn();
    render(
      <SearchBar
        searchInput={""}
        setSearchInput={setSearchInput}
        onClick={() => {}}
      />
    );

    userEvent.type(screen.getByTestId("search-input"), "Scream");

    await waitFor(() => {
      expect(setSearchInput).toHaveBeenCalledTimes(6);
      expect(setSearchInput).toHaveBeenNthCalledWith(1, "S");
      expect(setSearchInput).toHaveBeenNthCalledWith(2, "c");
    });
  });

  it("Calls onClick when search button is clicked", async () => {
    const handleClick = jest.fn();
    render(
      <SearchBar
        searchInput={"Star wars"}
        setSearchInput={() => {}}
        onClick={handleClick}
      />
    );

    userEvent.click(screen.getByTestId("search-btn"));

    await waitFor(() => {
      expect(handleClick).toHaveBeenCalledTimes(1);
    });
  });
});
