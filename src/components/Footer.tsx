import React from "react";
import styled from "styled-components";

export default function Footer() {
  return (
    <FooterContainer>
      <div>Made with ❤ by Adriana M.</div>
    </FooterContainer>
  );
}

const FooterContainer = styled.div`
  width: 100%;
  background-color: #202426;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding-top: 20px;
  padding-bottom: 20px;
  margin-top: 10px;
`;
