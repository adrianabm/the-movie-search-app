import React, { memo, useContext } from "react";
import styled from "styled-components";
import PrimaryButton from "../styled/PrimaryButton";
import { DarkModeContext } from "../../store/context";

interface PaginationProps {
  currentPage: number;
  setCurrentPage: (page: number) => void;
  lastPage: number;
}

function Pagination({
  currentPage,
  setCurrentPage,
  lastPage,
}: PaginationProps) {
  const context = useContext(DarkModeContext);

  const handleButtonClick = (page: number) => {
    return () => setCurrentPage(page);
  };

  return (
    <PaginationContainer>
      <PrimaryButton
        data-testid="pagination-btn-first"
        disabled={currentPage === 1}
        onClick={handleButtonClick(1)}
      >
        first
      </PrimaryButton>
      <PrimaryButton
        data-testid="pagination-btn-prev"
        disabled={currentPage === 1}
        onClick={handleButtonClick(currentPage - 1)}
      >
        previous
      </PrimaryButton>
      <PaginationNumber
        color={context.theme.foreground}
        data-testid="pagination-txt-current"
      >
        {currentPage}
      </PaginationNumber>
      <PrimaryButton
        data-testid="pagination-btn-next"
        disabled={currentPage === lastPage}
        onClick={handleButtonClick(currentPage + 1)}
      >
        next
      </PrimaryButton>
      <PrimaryButton
        data-testid="pagination-btn-last"
        disabled={currentPage === lastPage}
        onClick={handleButtonClick(lastPage)}
      >
        last
      </PrimaryButton>
    </PaginationContainer>
  );
}

export default memo(Pagination);

const PaginationNumber = styled.p`
  font-weight: 700;
  justify-content: center;
  font-size: 20px;
  align-items: center;
  padding-right: 20px;
  padding-left: 20px;
  color: ${(props) => props.color};
`;

const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  max-width: 600px;
  margin: auto;
  align-items: center;
  padding-top: 20px;
`;
