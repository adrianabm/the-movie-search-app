import React from "react";
import { MovieCardListWrapper, MovieListContainer } from "../styled/MovieList";
import MovieCardLoader from "./MovieCardLoader";

export default function MovieListLoader() {
  const placeHolderList: number[] = ([] = Array.from(Array(10).keys()));

  return (
    <MovieListContainer>
      <MovieCardListWrapper>
        {placeHolderList.map((id: number) => {
          return <MovieCardLoader key={id} />;
        })}
      </MovieCardListWrapper>
    </MovieListContainer>
  );
}
