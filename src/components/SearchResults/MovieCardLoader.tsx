import React, { useContext } from "react";
import placeholder from "../../assets/movie-placeholder.png";
import { DarkModeContext } from "../../store/context";
import MovieCardContainer from "../styled/MovieCardContainer";
import { MovieCardSummary, MovieTitle } from "../styled/MovieCard";

export default function MovieCardLoader() {
  const context = useContext(DarkModeContext);

  return (
    <MovieCardContainer>
      <img height="238" width="159" alt="movie-poster" src={placeholder}></img>
      <MovieCardSummary>
        <MovieTitle color={context.theme.foreground}>Loading...</MovieTitle>
      </MovieCardSummary>
    </MovieCardContainer>
  );
}
