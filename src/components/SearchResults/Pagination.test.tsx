import React from "react";
import { render, waitFor, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Pagination from "./Pagination";
import userEvent from "@testing-library/user-event";

let setCurrentPage: jest.Mock;

describe("Pagination", () => {
  describe("When currentPage is not the first, nor the last", () => {
    beforeEach(() => {
      setCurrentPage = jest.fn();

      render(
        <Pagination
          lastPage={5}
          currentPage={3}
          setCurrentPage={setCurrentPage}
        />
      );
    });

    it("Correctly changes currentPage value when 'Prev' button is clicked", async () => {
      userEvent.click(screen.getByTestId("pagination-btn-prev"));

      await waitFor(() => {
        expect(setCurrentPage).toHaveBeenCalledTimes(1);
        expect(setCurrentPage).toHaveBeenCalledWith(2);
      });
    });

    it("Correctly changes currentPage value when 'Next' button is clicked", async () => {
      userEvent.click(screen.getByTestId("pagination-btn-next"));

      await waitFor(() => {
        expect(setCurrentPage).toHaveBeenCalledTimes(1);
        expect(setCurrentPage).toHaveBeenCalledWith(4);
      });
    });

    it("Correctly changes currentPage value when 'First' button is clicked", async () => {
      userEvent.click(screen.getByTestId("pagination-btn-first"));

      await waitFor(() => {
        expect(setCurrentPage).toHaveBeenCalledTimes(1);
        expect(setCurrentPage).toHaveBeenCalledWith(1);
      });
    });

    it("Correctly changes currentPage value when 'Last' button is clicked", async () => {
      userEvent.click(screen.getByTestId("pagination-btn-last"));

      await waitFor(() => {
        expect(setCurrentPage).toHaveBeenCalledTimes(1);
        expect(setCurrentPage).toHaveBeenCalledWith(5);
      });
    });
  });

  describe("When currentPage is first", () => {
    it("Disables 'Prev' and 'First' buttons", async () => {
      const setCurrentPage = jest.fn();
      render(
        <Pagination
          lastPage={5}
          currentPage={1}
          setCurrentPage={setCurrentPage}
        />
      );

      expect(screen.getByTestId("pagination-btn-prev")).toBeDisabled;
      expect(screen.getByTestId("pagination-btn-first")).toBeDisabled;
    });
  });

  describe("When currentPage is last", () => {
    it("Disables 'Next' and 'Last' buttons", async () => {
      const setCurrentPage = jest.fn();
      render(
        <Pagination
          lastPage={5}
          currentPage={5}
          setCurrentPage={setCurrentPage}
        />
      );

      expect(screen.getByTestId("pagination-btn-next")).toBeDisabled;
      expect(screen.getByTestId("pagination-btn-last")).toBeDisabled;
    });
  });
});
