import React, { useCallback, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import movieApiClient from "../../utils/movieApiClient";
import { ImageSize, Movie } from "../../utils/typesApi";
import MovieCardContainer from "../styled/MovieCardContainer";
import { DarkModeContext } from "../../store/context";
import formatDate from "../../utils/formatDate";

interface MovieCardProps {
  movie: Movie;
}

export default function MovieCard({ movie }: MovieCardProps) {
  const navigate = useNavigate();
  const context = useContext(DarkModeContext);

  const onCardClick = useCallback(() => {
    return navigate(`/movie/${movie.id}`);
  }, [movie.id, navigate]);

  const imgSrc = useMemo(() => {
    return movieApiClient.buildMovieImageUrl(
      movie.poster_path,
      ImageSize.MEDIUM
    );
  }, [movie.poster_path]);

  const plotShorten = (text: string, length = 250) => {
    const shortText =
      text.length > length ? text.substring(0, length) + "..." : text;
    return shortText;
  };

  return (
    <MovieCardContainer data-testid="moviecard" onClick={onCardClick}>
      <img height="238" width="159" alt="movie-poster" src={imgSrc}></img>
      <MovieCardSummary>
        <MovieTitle
          data-testid="moviecard-title"
          color={context.theme.foreground}
        >
          {movie.title}
        </MovieTitle>

        <MovieDate
          data-testid="moviecard-date"
          color={context.theme.foreground}
        >
          Release Date: {formatDate(movie.release_date)}
        </MovieDate>
        <MoviePlot color={context.theme.foreground}>
          Plot: {plotShorten(movie.overview)}
        </MoviePlot>
      </MovieCardSummary>
    </MovieCardContainer>
  );
}

const MovieCardSummary = styled.div`
  padding: 10px;
  text-decoration: none;
  color: ${(props) => props.color};
`;

const MovieTitle = styled.h2`
  color: ${(props) => props.color};
`;

const MoviePlot = styled.p`
  color: ${(props) => props.color};
  text-decoration: none;
  height: 100px;
  overflow: hidden;
  font-size: 0.8em;
  // white-space: nowrap;
  text-overflow: ellipsis;
`;

const MovieDate = styled.p`
  color: ${(props) => props.color};
`;
