import React from "react";
import styled from "styled-components";

export default function AppLoader() {
  return (
    <AppLoaderContainer>
      <h1>Loading...</h1>
    </AppLoaderContainer>
  );
}

export const AppLoaderContainer = styled.div`
  height: 100vh;
`;
