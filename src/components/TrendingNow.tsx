import React, { memo, useEffect, useState } from "react";

import movieApiClient from "../utils/movieApiClient";
import { ApiError, isApiError, Movie } from "../utils/typesApi";
import MoviePosterGrid from "./MoviePoster/MoviePosterGrid";

function TrendingNow() {
  const [movieListTrending, setMovieListTrending] = useState<Movie[] | null>();
  const [error, setFetchError] = useState<ApiError | null>();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  function getTrending() {
    movieApiClient.getMovieListTrending().then((data) => {
      if (isApiError(data)) {
        setFetchError(data);
      } else {
        setMovieListTrending(data.results);
      }
      setIsLoading(false);
    });
  }

  useEffect(() => {
    getTrending();
  }, []);

  return (
    <MoviePosterGrid
      movieList={movieListTrending}
      headingText={"Trending Now"}
      error={error}
      isLoading={isLoading}
    />
  );
}

export default memo(TrendingNow);
