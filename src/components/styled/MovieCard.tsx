import React from "react";
import styled from "styled-components";

export const MovieCardSummary = styled.div`
  padding: 10px;
  text-decoration: none;
  color: ${(props) => props.color};
`;

export const MovieTitle = styled.h2`
  color: ${(props) => props.color};
`;

export const MoviePlot = styled.p`
  color: ${(props) => props.color};
  text-decoration: none;
  height: 100px;
  overflow: hidden;
  font-size: 0.8em;
  // white-space: nowrap;
  text-overflow: ellipsis;
`;

export const MovieDate = styled.p`
  color: ${(props) => props.color};
`;
