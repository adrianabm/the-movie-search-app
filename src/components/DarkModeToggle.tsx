import React, { useContext } from "react";
import Toggle from "react-toggle";
import styled from "styled-components";
import { DarkModeContext, themeList, ThemeName } from "../store/context";

export default function DarkModeToggle() {
  const context = useContext(DarkModeContext);

  const handleChange = () => {
    const themeName =
      context.theme === themeList.dark ? ThemeName.LIGHT : ThemeName.DARK;
    context.setTheme(themeName);
  };

  return (
    <ToggleContainer>
      <Toggle
        defaultChecked={context.theme === themeList.dark}
        onChange={handleChange}
      />
      <ToggleLabel>Dark Mode</ToggleLabel>
    </ToggleContainer>
  );
}

const ToggleContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ToggleLabel = styled.span`
  margin-left: 10px;
`;
