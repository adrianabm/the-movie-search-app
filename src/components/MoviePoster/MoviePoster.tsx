import React, { memo, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import movieApiClient from "../../utils/movieApiClient";
import { ImageSize, Movie } from "../../utils/typesApi";

function MoviePoster({ movieData }: { movieData: Movie }) {
  const navigate = useNavigate();

  const handleClick = useCallback(() => {
    return navigate(`/movie/${movieData.id}`);
  }, [movieData.id, navigate]);

  const imgSrc = useMemo(() => {
    return movieApiClient.buildMovieImageUrl(
      movieData.poster_path,
      ImageSize.SMALL
    );
  }, [movieData.poster_path]);

  return (
    <MoviePosterContainer>
      <MoviePosterImage
        src={imgSrc}
        height="174"
        width="112"
        onClick={handleClick}
      ></MoviePosterImage>
    </MoviePosterContainer>
  );
}

export default memo(MoviePoster);

const MoviePosterContainer = styled.div`
  margin-left: 2px;
  margin-right: 2px;
`;

const MoviePosterImage = styled.img`
  margin-left: 2px;
  margin-right: 2px;
  &:hover {
    cursor: pointer;
  }
`;
