import React from "react";
import styled from "styled-components";
import { MovieCardListWrapper, MovieListContainer } from "../styled/MovieList";

export default function MoviePosterGridLoader() {
  const placeHolderList: number[] = ([] = Array.from(Array(20).keys()));

  return (
    <>
      {placeHolderList.map((id: number) => {
        return <MoviePosterPlaceholder key={id} />;
      })}
    </>
  );
}

export const MoviePosterPlaceholder = styled.div`
  background-color: white;
  margin-left: 2px;
  margin-right: 2px;
  height: 174px;
  width: 112px;
`;
