import React, { memo } from "react";
import { ApiError, Movie } from "../../utils/typesApi";
import MoviePosterGridLoader from "./MoviePosterGridLoader";
import MoviePoster from "./MoviePoster";
import SectionHeading from "../styled/SectionHeading";
import styled from "styled-components";

interface MoviePosterGridProps {
  movieList: Movie[] | null | undefined;
  error: ApiError | null | undefined;
  headingText: string;
  isLoading: boolean;
}

function MoviePosterGrid({
  movieList,
  error,
  headingText,
  isLoading,
}: MoviePosterGridProps) {
  return (
    <section>
      <SectionHeading>{headingText}</SectionHeading>
      <CardGrid>
        {isLoading && <MoviePosterGridLoader />}
        {!error &&
          movieList?.map((movie) => (
            <MoviePoster movieData={movie} key={movie.id} />
          ))}
      </CardGrid>
      {error && <p>{error.message}</p>}
    </section>
  );
}

const CardGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 116px);
  grid-gap: 0.5rem;
  justify-content: space-between;
`;

export default memo(MoviePosterGrid);
