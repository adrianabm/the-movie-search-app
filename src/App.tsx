import React, { lazy, useState, Suspense } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import { DarkModeContext, Theme, themeList, ThemeName } from "./store/context";
import { AppContainer } from "./components/styled/AppContainer";
import AppLoader from "./components/AppLoader";

const LoadableMovieSearchPage = lazy(() => import("./views/MovieSearchPage"));
const LoadableMovieDetailPage = lazy(() => import("./views/MovieDetailPage"));

export default function App() {
  const [activeTheme, setActiveTheme] = useState(themeList.light);

  return (
    <DarkModeContext.Provider
      value={{
        theme: activeTheme,
        setTheme: (themeKey: ThemeName) => {
          setActiveTheme(themeList[themeKey] as Theme);
        },
      }}
    >
      <AppContainer>
        <Header></Header>
        <Router>
          <Suspense fallback={<AppLoader />}>
            <Routes>
              <Route path="/" element={<LoadableMovieSearchPage />} />
              <Route
                path="/movie/:id"
                element={<LoadableMovieDetailPage />}
              ></Route>
            </Routes>
          </Suspense>
        </Router>
        <Footer></Footer>
      </AppContainer>
    </DarkModeContext.Provider>
  );
}
